'''
Usage: app.py <fastq>

CAT STATS

Options:
    -h --help       This helpful helping of helpful help help help
'''

from collections import Counter
from docopt import docopt
from fabulous import text
import pysam

if __name__ == '__main__' :

    args = docopt(__doc__)

    cat_counts = Counter()
    with pysam.FastxFile(args['<fastq>']) as f :

        for read in f :
            cats = read.sequence.upper().count('CAT')
            cat_counts[cats] += 1

    print(text.Text('CAT STATS',shadow=True, skew=5))
    print('YOU HAVE {} CATS IN YOUR READS!!!'.format(
            sum(k*v for k,v in cat_counts.items())
        )
    )
    print('THAT\'S A LOT OF CATS!!')
    print('{} OF YOUR READS HAVE CATS IN THEM!!!'.format(
            sum(cat_counts.values())-cat_counts[0]
        )
    )
    print('YOU HAVE {} SAD CAT {} WITH NO CATS!!!'.format(
            cat_counts[0],
            ['READ','READS'][cat_counts[0]>1]
        )
    )
    print('MATH CAT SAYS YOU HAVE {} TOTAL READS THEN WOW!!!'.format(
            sum(cat_counts.values())
        )
    )
    print('MOST OF YOUR READS HAVE {} CATS IN THEM!!!'.format(
            cat_counts.most_common(1)[0][0]
        )
    )
    max_cats = cat_counts[max(cat_counts.keys())]
    print('THERE {} FULL OF CATS!!! '.format(
        ['IS ONE READ THAT IS', 'ARE {} READS THAT ARE'.format(max_cats)][max_cats>1]
        )
    )
    print('IT HAS {} CATS IN IT!!!'.format(max(cat_counts.keys())))
