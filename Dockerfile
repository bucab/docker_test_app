FROM python:3.6
WORKDIR /cwd
RUN apt install git
RUN git clone https://bitbucket.org/bubioinformaticshub/docker_test_app.git /app
RUN pip install -r /app/requirements.txt
ENTRYPOINT ["python","/app/app.py"]
